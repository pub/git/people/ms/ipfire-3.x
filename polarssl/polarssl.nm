###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = polarssl
version    = 1.3.9
release    = 1

groups     = System/Libraries
url        = http://polarssl.org/
license    = GPLv2+
summary    = Light-weight cryptographic and SSL/TLS library.

description
	PolarSSL is a light-weight open source cryptographic and SSL/TLS
	library written in C. PolarSSL makes it easy for developers to include
	cryptographic and SSL/TLS capabilities in their (embedded)
	applications with as little hassle as possible.

end

sources    = %{thisapp}-gpl.tgz
source_dl  = http://polarssl.org/code/releases/

build
	requires
		cmake
	end

	build
		%{cmake} \
			-D CMAKE_BUILD_TYPE:String="Release" \
			-D USE_SHARED_POLARSSL_LIBRARY:BOOL=1 \
			.

		make %{PARALLELISMFLAGS}
	end

	test
		LD_LIBRARY_PATH=$(pwd)/library ctest --output-on-failure -V
	end

	install_cmds
		mkdir -pv %{BUILDROOT}%{libexecdir}
		mv -v %{BUILDROOT}%{bindir} %{BUILDROOT}%{libexecdir}/%{name}
	end
end

packages
	package %{name}

	package %{name}-utils
		summary = PolarSSL utilities.
		description = %{summary}

		files
			%{libexecdir}/%{name}
		end
	end

	package %{name}-devel
		template DEVEL
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
